//
//  TaskTests.m
//  TaskTests
//
//  Created by Nguyen Hoang Tuan on 5/9/17.
//  Copyright © 2017 Elias Pietilä. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface TaskTests : XCTestCase

@end

@implementation TaskTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testSwapString {
    NSArray *inputArray = @[@"Change #This# to be #That#", @"#A# #B#", @"  #A#  #B#  ", @"#A#", @"#A #B", @"A #B#", @"#A #B#"];
    NSArray *rightArray = @[@"Change #That# to be #This#", @"#B# #A#", @"  #B#  #A#  ", @"#A#", @"#A #B", @"A #B#", @"#A #B#"];
    for (int i = 0; i < inputArray.count; i++) {
        NSString *resultString = [ViewController swapSubString:inputArray[i]];
        XCTAssertTrue([resultString isEqualToString:rightArray[i]]);
    }
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
