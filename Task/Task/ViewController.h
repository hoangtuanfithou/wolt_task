//
//  ViewController.h
//  Task
//
//  Created by Elias Pietilä on 18/04/16.
//  Copyright © 2016 Elias Pietilä. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

+ (NSString *)swapSubString:(NSString *)inputString;

@end

