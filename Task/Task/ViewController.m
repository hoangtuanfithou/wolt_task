//
//  ViewController.m
//  Task
//
//  Created by Elias Pietilä on 18/04/16.
//  Copyright © 2016 Elias Pietilä. All rights reserved.
//

#import "ViewController.h"
#import <Intercom/Intercom.h>

@interface ViewController ()

@end

@implementation ViewController

#define kKeyword @"Hey there. Change "
BOOL isChangedText = false;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openIntercom:(id)sender {
    [Intercom presentMessageComposer];
    
    // Solution 1
//    [self performSelector:@selector(changeText) withObject:nil afterDelay:0.6];
    
    // Soution 2
    if (!isChangedText) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeText) name:IntercomWindowWillShowNotification object:nil];
    }
}

#pragma mark - Change text #This# to be #That#

/**
 Change text of label in presend message composer view
 */
- (void)changeText {
    UIViewController *view = [UIApplication sharedApplication].keyWindow.rootViewController;
    [view addObserver:self forKeyPath:@"controllerToPresent" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
    [self findLabel:view.presentedViewController.view withKeyWord:kKeyword];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"controllerToPresent"]) {
        if ([[change objectForKey:NSKeyValueChangeOldKey] isKindOfClass:[UISplitViewController class]]) {
            UISplitViewController *splitView = (UISplitViewController *)[change objectForKey:NSKeyValueChangeOldKey];
            for (UIViewController *vc in splitView.viewControllers) {
                if ([vc isKindOfClass:[UINavigationController class]]) {
                    for (UIViewController *subView in [(UINavigationController *)vc viewControllers]) {
                        [self findLabel:subView.view withKeyWord:kKeyword];
                    }
                }
            }
        }
        [object removeObserver:self forKeyPath:@"controllerToPresent"];
    }
}

/**
 Check View and all subviews to find UILabel with text contains input keyword

 @param view : UIView
 @param keyword : a string keyword
 */
- (void)findLabel:(UIView *)view withKeyWord:(NSString *)keyword{
    if ([view isKindOfClass:[UILabel class]]) {
        UILabel *lbl = (UILabel *)view;
        if ([lbl.text containsString:keyword]) {
            UIFont *textFont = lbl.font;
            UIColor *textColor = lbl.textColor;
            
            lbl.alpha = 0.0;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                lbl.text = [ViewController swapSubString:lbl.text];
                lbl.alpha = 1.0;
                lbl.font = textFont;
                lbl.textColor = textColor;
                isChangedText = true;
            });
        }
    }
    
    for (UIView *subview in view.subviews) {
        [self findLabel:subview withKeyWord:kKeyword];
    }
}

/**
 Swap sub string for input string EX: swap #This# and #That#

 @param inputString : string for swap
 @return swapped string
 */
+ (NSString *)swapSubString:(NSString *)inputString {
    NSMutableArray *hashtagWordsArray = [NSMutableArray arrayWithArray:[inputString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    NSPredicate *hashtagDetectionPredicate = [NSPredicate predicateWithFormat:@"(SELF BEGINSWITH %@) AND (SELF ENDSWITH %@)", @"#", @"#"];
    [hashtagWordsArray filterUsingPredicate:hashtagDetectionPredicate];
    
    if (hashtagWordsArray.count > 1) {
        NSString *thisString = hashtagWordsArray[0];
        NSString *thatString = hashtagWordsArray[1];
        NSString *placeHolderString = @"#placeHolderString#";
        inputString = [inputString stringByReplacingOccurrencesOfString:thisString withString:placeHolderString];
        inputString = [inputString stringByReplacingOccurrencesOfString:thatString withString:thisString];
        inputString = [inputString stringByReplacingOccurrencesOfString:placeHolderString withString:thatString];
    }
    return inputString;
}

@end
